��          L       |       |   S   }      �      �   &   �        �  4  ]   �     ;     R  8   [     �   A simple plugin that show you if the options page has been saved at least one time. ACF Options Page Saved Kofinorr This options page has not been saved ! https://github.com/kofinorr Project-Id-Version: ACF Options Page Saved
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-01-16 13:35+0000
PO-Revision-Date: 2021-01-16 13:36+0000
Last-Translator: 
Language-Team: Français
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.0; wp-5.6
X-Domain: krr-aops Un plugin simple pour savoir si la page d'options ACF a au moins été enregistrée une fois. ACF Options Page Saved Kofinorr Cette page d'options n'a pas encore été sauvegardée ! https://github.com/kofinorr 