#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ACF Options Page Saved\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-01-16 13:35+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.0; wp-5.6\n"
"X-Domain: krr-aops"

#. Description of the plugin
msgid ""
"A simple plugin that show you if the options page has been saved at least "
"one time."
msgstr ""

#. Name of the plugin
msgid "ACF Options Page Saved"
msgstr ""

#. Author URI of the plugin
msgid "https://github.com/kofinorr"
msgstr ""

#. Author of the plugin
msgid "Kofinorr"
msgstr ""

#: acf-options-page-saved.php:119
msgid "This options page has not been saved !"
msgstr ""
